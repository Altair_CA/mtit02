import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JButton;

import com.mtit.models.User;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainApplication {

	private JFrame frame;
	private Login loginWindow;
	private User loggedUser;
	private Transaction transaction;
	private Registration registrationWindow;
	private static MainApplication window;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new MainApplication();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainApplication() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("unused")
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 229, 86);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton_2 = new JButton("Register");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				registrationWindow = new Registration();
				registrationWindow.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				registrationWindow.setVisible(true);
			}
		});
		btnNewButton_2.setBounds(10, 11, 96, 32);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				loginWindow = new Login();
				loginWindow.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				loginWindow.setVisible(true,window);
			}
		});
		btnLogin.setBounds(116, 11, 96, 32);
		frame.getContentPane().add(btnLogin);
		JButton btnNewButton = new JButton("Registration");
		JButton btnNewButton_1 = new JButton("Login");
		
	}
	public void LoginSucess(User user){
		this.loggedUser = user;
		loginWindow.dispose();
		loginWindow = null;
		System.out.println("Login success");
		
		this.transaction = new Transaction();
		this.transaction.setUser(this.loggedUser);
		transaction.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		transaction.setVisible(true,window);
		window.frame.setVisible(false);
	}
	public void setVisible(boolean _visible){
		window.frame.setVisible(_visible);
	}
}
