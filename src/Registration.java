import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import com.mtit.service.UserManagement;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("serial")
public class Registration extends JDialog {
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private UserManagement userManagement = new UserManagement();
	/**
	 * Launch the application.
	 */
	/**
	 * Create the dialog.
	 */
	public Registration() {
		setBounds(100, 100, 307, 166);
		getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(124, 11, 135, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 14, 73, 14);
		getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 45, 55, 14);
		getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(124, 42, 135, 20);
		getContentPane().add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(124, 67, 135, 20);
		getContentPane().add(passwordField_1);
		
		JLabel lblRePassword = new JLabel("Re Password");
		lblRePassword.setBounds(10, 70, 73, 14);
		getContentPane().add(lblRePassword);
		
		JButton btnNewButton = new JButton("Register");
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				try{
					if(textField.getText().trim().isEmpty() || passwordField.getText().trim().isEmpty() || passwordField_1.getText().trim().isEmpty()){
						JOptionPane.showMessageDialog(null, "Fields cannot be empty");
						
					}else{
						if(userManagement.registration(textField.getText().trim(), passwordField.getText().trim())){
							JOptionPane.showMessageDialog(null, "Success");
							dispose();
						}else if(!passwordField.getText().contentEquals(passwordField_1.getText())){
							JOptionPane.showMessageDialog(null, "Passwords must be matched");
						}
						else{
							JOptionPane.showMessageDialog(null, "Registration Failed");
						}
					}
				}catch(Exception ex){
					JOptionPane.showMessageDialog(null, ex);
				}
			}
		});
		btnNewButton.setBounds(124, 93, 135, 23);
		getContentPane().add(btnNewButton);

	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		super.dispose();
	}
}
