package com.mtit.aspects;

import com.mtit.models.User;
import com.mtit.service.InsufficiantFundsException;

public aspect Logging {
	/*
	 * 
	 * Login log
	 */
	after(String userName,String password) returning(Object returnVal):
		(execution(* com.mtit.service.UserManagement.login(String,String)) 
				&& args(userName,password) )
		{
		if(returnVal == null){
			System.out.println("User " + userName+" login fail" );
		}else{
			System.out.println("User " + userName+" logged successful" );
		}
		
	}
	
	/*
	 * Logging before deposit
	 */
	before(User user,double amount):
		(execution(* com.mtit.service.TransactionManagment.deposite(User,double))
				&& args(user,amount) )
	{
		System.out.println(user.username +" is request to deposit " + amount);
		
	}
	
	/*
	 * Success Deposit log
	 */
	after(User user,double amount) returning(Object returnVal): 
		(execution(* com.mtit.service.TransactionManagment.deposite(User,double))
				&& args(user,amount) )
	{
		System.out.println(user.username+" has deposited  " + returnVal.toString());
	}
	
	/*
	 * Before width draw
	 */
	before(User user,double amount):
		(execution(* com.mtit.service.TransactionManagment.widthDraw(User,double))
				&& args(user,amount) )
	{
		System.out.println(user.username +" is request to withdraw " + amount);
		
	}
	
	/*
	 * after width draw
	 * 
	 */
	after(User user,double amount) returning(Object returnVal): 
		(execution(* com.mtit.service.TransactionManagment.widthDraw(User,double)) 
				&& args(user,amount) )
	{
		System.out.println(user.username+" has withdrawed  " + amount);
	}
	
	/*
	 * Insufficient funds log
	 */
	after(User user,double amount) throwing(InsufficiantFundsException ex): 
		(execution(* com.mtit.service.TransactionManagment.widthDraw(User,double)) 
				&& args(user,amount) )
	{
		System.out.println(user.username + " has innsuffician funds : " +ex);
	}
	
	/*
	 * 
	 * 
	 */
	after() throwing(Exception ex): 
		execution(* *(..))
				
	{
		System.out.println("error occured " +ex);
	}
}
