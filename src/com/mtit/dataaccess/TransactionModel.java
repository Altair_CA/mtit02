package com.mtit.dataaccess;

import java.sql.Connection;
import java.sql.Statement;

import com.mtit.models.User;

public class TransactionModel {
	public static double deposite(User _user,double amount) throws Exception{
			Connection conn = SqlConnection.dbConnector();
			Statement statement;
			
			statement = conn.createStatement();
			statement.setQueryTimeout(30);
			statement.executeUpdate("update user set balance="+(_user.balance+amount)+" where id="+_user.id);
			statement.executeUpdate("insert into transactions values("+_user.id+","+_user.id+","+amount+")");
			
			return _user.balance+amount;
	}
	public static double widthDraw(User _user,double amount) throws Exception{
		Connection conn = SqlConnection.dbConnector();
		double remaining = _user.balance - amount;
		if(remaining < 0){
			return 0;
		}
		Statement statement;
		statement = conn.createStatement();
		statement.setQueryTimeout(30);
		statement.executeUpdate("update user set balance="+(remaining)+" where id="+_user.id);
		statement.executeUpdate("insert into transactions values("+_user.id+","+_user.id+","+(amount*-1)+")");
		return remaining;
	
	}
	
}
