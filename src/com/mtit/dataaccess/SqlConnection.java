package com.mtit.dataaccess;

import java.sql.*;

import javax.swing.JOptionPane;

public class SqlConnection {
	private static Connection conn = null;
	
	public static Connection dbConnector(){
		try{
			Class.forName("org.sqlite.JDBC");
			if(conn == null || conn.isClosed()){
				conn = DriverManager.getConnection("jdbc:sqlite::resource:MTIT.sqlite");
			}
			return conn;
		}catch(Exception ex){
			JOptionPane.showMessageDialog(null, ex);
			return null;
		}
	}
}

