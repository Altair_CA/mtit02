package com.mtit.dataaccess;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mtit.models.User;

public class UserModel {
	public static User get(String username){
		Connection conn = SqlConnection.dbConnector();
		Statement statement;
	try {
			statement = conn.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("select * from user where username='"+username+"'");
			if(rs.next()){
				return new User(
						rs.getInt("id"),
						rs.getString("username"),
						rs.getString("password"),
						rs.getDouble("balance"),
						rs.getInt("level")
						);
			}else{
				return null;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public static boolean addUser(String username,String password){
		try{
			Connection conn = SqlConnection.dbConnector();
			Statement statement;
			statement = conn.createStatement();
			statement.setQueryTimeout(30);
			ResultSet rs = statement.executeQuery("select * from user");
			int count = 0;
			while(rs.next()){
				count++;
			}
			count++;
			statement.executeUpdate("insert into user values("+ count +",'"+username+"','"+password+"',0,0)");
			return true;
		}catch(Exception ex){
			System.out.print(ex);
			return false;
		}
	}
}
