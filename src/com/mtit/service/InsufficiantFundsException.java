package com.mtit.service;

public class InsufficiantFundsException extends Exception {
	public InsufficiantFundsException(String msg){
		super(msg);
	}
}
