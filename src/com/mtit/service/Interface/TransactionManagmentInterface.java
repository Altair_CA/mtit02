package com.mtit.service.Interface;

import com.mtit.models.User;

public interface TransactionManagmentInterface {
	public double widthDraw(User _user,double amount) throws Exception;
	public double deposite(User _user,double amount);
}
