package com.mtit.service.Interface;

import com.mtit.models.User;

public interface UserManagmentInterface {
	public User login(String userName,String password);
	public boolean registration(String userName,String password);
}
