package com.mtit.service;

import com.mtit.dataaccess.TransactionModel;
import com.mtit.models.User;
import com.mtit.service.Interface.TransactionManagmentInterface;

public class TransactionManagment implements TransactionManagmentInterface {
	public double widthDraw(User _user,double amount) throws InsufficiantFundsException{
		if(_user.balance<amount){
			throw new InsufficiantFundsException("Insuficiant funds");
		}
		try {
			return TransactionModel.widthDraw(_user, amount);
		} catch (Exception e) {
			return 0;
			// TODO Auto-generated catch block
			
		}
	}
	public double deposite(User _user,double amount) {
		try {
			return TransactionModel.deposite(_user, amount);
		} catch (Exception e) {
			return -1;
			// TODO Auto-generated catch block
			
		}
	}
}
