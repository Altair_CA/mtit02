import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.mtit.models.User;
import com.mtit.service.TransactionManagment;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


@SuppressWarnings("serial")
public class Transaction extends JDialog {
	User user = null;
	MainApplication mainApp=null;
	JLabel lblNewLabel = null;
	JButton btnViewLog;
	TransactionManagment transactionManagment = new TransactionManagment();
	/**
	 * Create the dialog.
	 */
	public Transaction() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				mainApp.setVisible(true);
			}
		});
		setBounds(100, 100, 294, 161);
		getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("New label");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 97, 258, 14);
		getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Deposit");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double amount = Double.valueOf(JOptionPane.showInputDialog("Enter the amount to deposite"));
				user.balance = transactionManagment.deposite(user, amount);
				lblNewLabel.setText(String.valueOf(user.balance));
			}
		});
		btnNewButton.setBounds(154, 34, 114, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Widthdraw");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double amount = Double.valueOf(JOptionPane.showInputDialog("Enter the amount to widthdraw"));
				try {
					user.balance = transactionManagment.widthDraw(user, amount);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lblNewLabel.setText(String.valueOf(user.balance));
			}
		});
		btnNewButton_1.setBounds(10, 34, 114, 23);
		getContentPane().add(btnNewButton_1);
		
		btnViewLog = new JButton("View Log");
		btnViewLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btnViewLog.setBounds(88, 68, 114, 23);
		

	}
	public void setUser(User _user){
		this.user = _user;
		lblNewLabel.setText(String.valueOf(this.user.balance));
		if(user.level >=10){
			//getContentPane().add(btnViewLog);
		}
		
	}
	public void setVisible(boolean visible,MainApplication mainWIndow){
		this.setVisible(visible);
		this.mainApp = mainWIndow;
		
	}
}
